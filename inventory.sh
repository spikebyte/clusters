#!/bin/sh
set -e
mkdir -p terraform.tfstate.d
terraform state pull > terraform.tfstate.d/tmp.tfstate
export TERRAFORM_STATE_ROOT="terraform.tfstate.d/"
python kubespray/contrib/terraform/terraform.py "$@"
