#!/bin/sh
OIDC_CLIENT_ID=830d4536f02b442ffc4d63b44e8859e04d919e89b0d2a688202f0a5e6572562f
jinja2 template.yaml.j2 ../clouds.yaml > template.yaml

echo "Kuberos is available at: http://localhost:10003/"
docker run -p 10003:10003 -v $PWD:/cfg "negz/kuberos:latest" \
  /kuberos --scopes='' https://gitlab.com $OIDC_CLIENT_ID /cfg/secret /cfg/template.yaml
