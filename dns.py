#!/usr/bin/env python
import argparse
import ovh
from pprint import pprint


parser = argparse.ArgumentParser(description='Update DNS A records.')
parser.add_argument('action', choices=['add', 'delete', 'show'], help='')
parser.add_argument('ip', help='')
parser.add_argument('--zone', required=True, help='')
parser.add_argument('--subdomain', required=True, default='', help='')
parser.add_argument('--ttl', type=int, default=60,
                    help='Default TTL when adding records')

args = parser.parse_args()

client = ovh.Client(endpoint='ovh-eu')

base = '/domain/zone/' + args.zone
refresh_needed = False

if args.action == 'add':
    print('Adding %s.%s %s ' % (args.subdomain, args.zone, args.ip))
    response = client.post(base + '/record', fieldType='A', ttl=args.ttl,
                           target=args.ip, subDomain=args.subdomain)
    refresh_needed = True
else:
    records = client.get(base + '/record', fieldType='A',
                         subDomain=args.subdomain)
    for record_id in records:
        record = client.get(base + '/record/' + str(record_id))

        if args.action == 'show':
            print('%(subDomain)s.%(zone)s %(target)s (%(ttl)ds)' % record)

        if args.action == 'delete' and record['target'] == args.ip:
            print('Deleting %s.%s %s ' % (args.subdomain, args.zone, args.ip))
            client.delete(base + '/record/' + str(record_id))
            refresh_needed = True

# Apply changes
if refresh_needed:
    client.post(base + '/refresh')
