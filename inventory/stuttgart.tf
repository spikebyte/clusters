# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"

az_list = ["south-1"]

# masters
flavor_k8s_master = "260" # 4C-8GB-10GB

# networking
external_net = "0647c0a0-862c-4c7e-9433-4558fcc5573b"
floatingip_pool = "public"
