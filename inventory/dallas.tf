# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 LTS (Xenial) 64 Bit [20180126-1]"

az_list = ["us-dfw-1"]

# masters
flavor_k8s_master = "a7a482a9-f77c-4fae-8d10-315600665ffc" # l1.medium
number_of_k8s_masters = 0
number_of_k8s_masters_no_floating_ip = 1

# networking
network_name = "Public Internet"
external_net = "5040f22e-0254-4be3-95cb-fdf7e1c32517"
floatingip_pool = "Public Internet"
dns_nameservers = []
use_neutron = 0
