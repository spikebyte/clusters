# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 LTS (Xenial) 64 Bit [20180215-1]"

az_list = ["us-slc-1"]

# masters
flavor_k8s_master = "49f3da64-f8a2-4058-aa3b-cdf77607ab58" # s1.medium
number_of_k8s_masters = 0
number_of_k8s_masters_no_floating_ip = 1

# networking
network_name = "Public Internet"
external_net = "db5d9d4d-74d9-4dee-998c-389be8bf4739"
floatingip_pool = "Public Internet"
dns_nameservers = []
use_neutron = 0
