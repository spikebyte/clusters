# image to use for bastion, masters, standalone etcd instances, and nodes
image = "xenial-server-cloudimg-amd64-disk1.img"

# masters
flavor_k8s_master = "4" # m1.large

# networking
external_net = "11abf603-d3e7-4f38-98c2-bf91311c9936"
floatingip_pool = "external"
