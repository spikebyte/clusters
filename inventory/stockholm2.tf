# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 Xenial Xerus"

# masters
flavor_k8s_master = "952c6318-a2bf-4c8e-b57e-7ed29dde9b60" # 1C-4GB-20GB

# networking
external_net = "3e4a9105-7b10-4186-a053-dc9373bba0f4"
floatingip_pool = "ext-net"
