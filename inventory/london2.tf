# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"

# masters
flavor_k8s_master = "defa64c3-bd46-43b4-858a-d93bbae0a229" # s1-8
number_of_k8s_masters = 0
number_of_k8s_masters_no_floating_ip = 1

# networking
network_name = "Ext-Net"
external_net = "6011fbc9-4cbf-46a4-8452-6890a340b60b"
floatingip_pool = "Ext-Net"
dns_nameservers = []
use_neutron = 0
