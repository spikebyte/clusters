# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 Xenial Xerus"

# masters
flavor_k8s_master = "7d3825c0-32ba-4b9e-8d00-00a9694e58ce" # 2C-8GB-50GB

# networking
external_net = "6fc73bfd-c0b5-418c-84a1-ffa719257ad1"
floatingip_pool = "ext-net"
