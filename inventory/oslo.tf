# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 (AMD64) [Local Storage]"

az_list = ["az1"]

# masters
flavor_k8s_master = "7"

# networking
external_net = "2acd57f2-a283-4f0f-86cf-1174afab1431"
floatingip_pool = "Public"
