# image to use for bastion, masters, standalone etcd instances, and nodes
image = "ubuntu-16.04-server-20180912"

az_list = ["sto1"]

# masters
flavor_k8s_master = "3f73fc93-ec61-4808-88df-2580d94c1a9b"

# networking
external_net = "600b8501-78cb-4155-9c9f-23dfcba88828"
floatingip_pool = "elx-public1"
